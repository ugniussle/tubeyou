<?php

namespace App\Models;

use CodeIgniter\Model;

class VideosModel extends Model{
    protected $table = 'videos';

    protected $allowedFields = ['url','title','filename','description','user_id','thumbnail','views','likes','dislikes','url','upload_date'];

    public function getVideos($url = false){
        $videos = NULL;

        if($url == false){
            $videos = $this->findAll();
        }
        else{
            $videos = [$this->where(['url'=>$url])->first()];
        }

        $newVideos = [];

        foreach ($videos as $video){
            $id = $video['user_id'];
            $video['uploader'] = model(UserModel::class)->getUserName($id);
            array_push($newVideos, $video);
        }

        return $newVideos;
    }

    public function getUserVideos($id){
        if(($user ?? '') == ''){
            return [];
        }

        return $this->where(['user_id'=>$id])->findAll();
    }

    public function addVideo($title, $url, $filename, $user_id, $thumbnail, $description, $visibility){
        $this->save([
            'title' => $title,
            'url' => $url,
            'filename' => $filename,
            'user_id' => $user_id,
            'thumbnail'=> $thumbnail,
            'description'=>$description,
            'visiblity'=>$visibility,
        ]);
    }

    public function increaseViews($url){
        $row = $this->where(['url'=>$url])->first();
        $row['views']++;
        $this->update($row['id'], $row);
    }
}
?>