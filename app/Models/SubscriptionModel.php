<?php
namespace App\Models;

use CodeIgniter\Model;

class subscriptionModel extends Model{
    protected $table = 'subscriptions';

    protected $allowedFields = ['user_id', 'recipient_id'];

    public function getInfo(){
        return $this->findAll();
    }
}
?>