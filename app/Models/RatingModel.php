<?php

namespace App\Models;

use CodeIgniter\Model;

class ratingModel extends Model{
    protected $table = 'ratings';

    protected $primaryKey = ['user_id', 'video_id'];

    protected $allowedFields = ['user_id','video_id','type'];

    public function getInfo(){
        return $this->findAll();
    }

    public function getRatingType($user_id, $video_id){
        $db = db_connect();

        $query = $db->table('ratings')->select('type');

        $result = $query->getWhere(['user_id' => $user_id, 'video_id' => $video_id], 1);

        if(isset($result->getFirstRow('array')['type']))
            $type = $result->getFirstRow('array')['type'];
        else
            $type = null;

        return $type;
    }

    /**
     * If failed, returns false, if succeeded, returns true.
     * @param int $user_id
     * @param int $video_id
     * @param int $rating
     * @return bool
     *
     */
    public function insertRating($user_id, $video_id, $rating){
        $db = db_connect();
        $result = $db->table('ratings')->insert(['user_id'=>$user_id, 'video_id'=>$video_id, 'type' => $rating]);
        $db->close();

        return $result;
    }

    public function deleteRating($user_id, $video_id){
        $db = db_connect();
        $result = $db->table('ratings')->delete(['user_id'=>$user_id, 'video_id'=>$video_id]);
        $db->close();

        return $result;
    }

    /**
     * Returns array of like count and dislike count.
     * @param int $video_id
     * @return array
     */
    public function calculateRatings($video_id){
        $db = db_connect();

        $query = $db->table('ratings')->select(['video_id', 'type']);

        $result = $query->getWhere(['video_id' => $video_id]);

        $likes = 0;
        $dislikes = 0;

        foreach($result->getResultArray() as $row){
            if($row['type'] == true){
                $likes++;
            }
            else if($row['type'] == false){
                $dislikes++;
            }
        }

        $db->close();

        return ['likes' => $likes, 'dislikes' => $dislikes];
    }

    public function updateRatings($video_id){
        $ratings = $this->calculateRatings($video_id);

        $videosModel = new VideosModel();

        $video = $videosModel->find($video_id);
        if($video){
            $video['likes'] = $ratings['likes'];
            $video['dislikes'] = $ratings['dislikes'];
            $ret = $videosModel->update($video_id, $video);
            
            if($ret)
                log_message('debug', 'ratings updated');
            else
                log_message('error', 'ratings not updated');
        }
        else{
            log_message('error', 'video not found');
        }
    }
}
?>