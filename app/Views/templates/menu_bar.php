<?php if(!empty($menu) && is_array($menu)): ?>
    <style>
        @keyframes menu_expand{
            to{height:7%};
        }
        @keyframes menu_item_color{
            25%{color:lightgreen;}
            50%{color:yellow;}
            75%{color:blueviolet;}
        }
        @keyframes shrink_body{
            to{height:93%};
        }
        body{
            margin:0;
        }
        .menu-item{
            font-size: 2em;
            color:white;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100%;
        }
        .menu>a{
            height: auto;
            width: <?= 100/count($menu).'%'; ?>;
            text-align: center;
            text-decoration: none;
            vertical-align: middle; 
        }
        .menu>a:hover{
            background-color: black;
        }
        .menu-item:hover{
            animation: menu_item_color 2s infinite;
            animation-fill-mode: forwards;
            animation-timing-function: linear;
        }
        .menu{
            height:5%;
            display: flex;
            background-color: rgb(50,50,50);
        }
        .menu:hover{
            animation: menu_expand 0.7s;
            animation-timing-function: ease;
            animation-fill-mode: forwards;
        }
        <?php if($sidebar): ?>
            .menu:hover ~ .sidebarRight{
                animation: shrink_sidebar 0.7s;
                animation-timing-function: ease;
                animation-fill-mode: forwards;
            }
        <?php endif ?>
        .menu:hover ~ .body-container{
            animation: shrink_body 0.7s;
            animation-timing-function: ease;
            animation-fill-mode: forwards;
        }
    </style>
    <div class="menu">
        <?php foreach($menu as $menu_item): ?>
            
            <a href="<?=esc($menu_item['link']) ?>"> 
                <div class="menu-item" <?php if(($title ?? '') == $menu_item['name']) echo "style='color:red'" ?>> <?=esc($menu_item['name'])?> </div>
            </a>

        <?php endforeach; ?>
    </div>

<?php endif ?>