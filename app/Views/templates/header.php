<!DOCTYPE html>
<html>
    <body>
        <?php 
        if(isset($message) && $message!=''){
            echo '<p style="color:red;font-size:2em">'.esc($message).'</p>';
        }
        ?>
        <h1 class='header-title' style='margin-top:0'> <?= esc($title) ?> </h1>
    </body>
</html>
