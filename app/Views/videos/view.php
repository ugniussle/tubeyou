<head>
  <link href="https://vjs.zencdn.net/7.17.0/video-js.css" rel="stylesheet" />

    <style>
        #player{
            width: 100%;
            height: 93%;
        }
        .body-container{
            background-color: rgb(30,30,30);
        }
        .video-data{
            width: 65%;
        }
        .description{
            text-align: center;
            margin-top: 30px !important;
        }
        .title{
            padding-left: 10px;
            font-size: 2em !important;
            width: 99% !important;
        }
        .row{
            display: flex;
            flex-direction: row;
        }
        .video-data-item{
            margin:0;
            color:white;
            font-size: 1.2em;
        }
        .views-date{
            padding-right: 10px;
            padding-left: 10px;
        }
        .buttons{
            right: 0;
            margin-left: 50%;
        }
    </style>

</head>

<body>

    <video-js
        id="player"
        class="video-js vjs-big-play-centered player"
        controls
        preload="auto"
        poster="<?= base_url('Files/Thumbnails/'.$video['thumbnail']) ?>"
        data-setup="{}">

        <source src="<?= base_url('Files/Videos/'.$video['filename']) ?>" type="video/mp4" />
    </video-js>
    <div class='video-data'>
        <div class='title video-data-item'><?= $video['title'] ?></div>
        <div class='row'>
            <div class='video-data-item views-date'><?= $video['views'] ?> views | <?= $video['upload_date'] ?></div>
            <div class='buttons'>
                <button id='like' onclick='rateVideo(true)'><div id='likeCount'><?= $video['likes'] ?></div> Like</button>
                <button id='dislike' onclick='rateVideo(false)'><div id='dislikeCount'><?= $video['dislikes'] ?></div> Dislike</button>
                <button>Add to playlist</button>
            </div>
        </div>
        <div class='description video-data-item'>
            <center class='' style='font-size:1.8em'>Description:</center>
            <center class=''><?= $video['description'] ?></p>
        </div>
        <div class='comments video-data-item'>
            <center class='' style='font-size:1.8em'>Comments:</center>
            <div style='margin-left:5px'>
                
            </div>
        </div>
    </div>
    
    <script src="https://vjs.zencdn.net/7.17.0/video.min.js"></script>
    <script>
        "use strict";
        (function(factory) {
        /*!
        * Custom Universal Module Definition (UMD)
        *
        * Video.js will never be a non-browser lib so we can simplify UMD a bunch and
        * still support requirejs and browserify. This also needs to be closure
        * compiler compatible, so string keys are used.
        */
        if (typeof define === 'function' && define['amd']) {
            define(['video.js'], function(vjs){ factory(window, document, vjs) });
        // checking that module is an object too because of umdjs/umd#35
        } else if (typeof exports === 'object' && typeof module === 'object') {
            factory(window, document, require('video.js'));
        } else {
            factory(window, document, videojs);
        }

        })(function(window, document, vjs) {
        //cookie functions from https://developer.mozilla.org/en-US/docs/DOM/document.cookie
        var
        getCookieItem = function(sKey) {
            if (!sKey || !hasCookieItem(sKey)) { return null; }
            var reg_ex = new RegExp(
            "(?:^|.*;\\s*)" +
            window.escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") +
            "\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*"
            );
            return window.unescape(document.cookie.replace(reg_ex,"$1"));
        },

        setCookieItem = function(sKey, sValue, vEnd, sPath, sDomain, bSecure) {
            if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return; }
            var sExpires = "";
            if (vEnd) {
            switch (vEnd.constructor) {
                case Number:
                sExpires = vEnd === Infinity ? "; expires=Tue, 19 Jan 2038 03:14:07 GMT" : "; max-age=" + vEnd;
                break;
                case String:
                sExpires = "; expires=" + vEnd;
                break;
                case Date:
                sExpires = "; expires=" + vEnd.toGMTString();
                break;
            }
            }
            document.cookie =
            window.escape(sKey) + "=" +
            window.escape(sValue) +
            sExpires +
            (sDomain ? "; domain=" + sDomain : "") +
            (sPath ? "; path=" + sPath : "") +
            (bSecure ? "; secure" : "");
        },

        hasCookieItem = function(sKey) {
            return (new RegExp(
            "(?:^|;\\s*)" +
            window.escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") +
            "\\s*\\=")
            ).test(document.cookie);
        },

        hasLocalStorage = function() {
            try {
            window.localStorage.setItem('persistVolume', 'persistVolume');
            window.localStorage.removeItem('persistVolume');
            return true;
            } catch(e) {
            return false;
            }
        },
        getStorageItem = function(key) {
            return hasLocalStorage() ? window.localStorage.getItem(key) : getCookieItem(key);
        },
        setStorageItem = function(key, value) {
            return hasLocalStorage() ? window.localStorage.setItem(key, value) : setCookieItem(key, value, Infinity, '/');
        },

        extend = function(obj) {
            var arg, i, k;
            for (i = 1; i < arguments.length; i++) {
            arg = arguments[i];
            for (k in arg) {
                if (arg.hasOwnProperty(k)) {
                obj[k] = arg[k];
                }
            }
            }
            return obj;
        },

        defaults = {
            namespace: ""
        },

        volumePersister = function(options) {
            var player = this;
            var settings = extend({}, defaults, options || {});

            var key = settings.namespace + '-' + 'volume';
            var muteKey = settings.namespace + '-' + 'mute';

            player.on("volumechange", function() {
            setStorageItem(key, player.volume());
            setStorageItem(muteKey, player.muted());
            });

            player.ready(function() {
            var persistedVolume = getStorageItem(key);
            if(persistedVolume !== null) {
                player.volume(persistedVolume);
            }

            var persistedMute = getStorageItem(muteKey);
            if(persistedMute !== null) {
                player.muted('true' === persistedMute);
            }
            });
        };

        vjs.plugin("persistvolume", volumePersister);

        });
    </script>

    <script>
        var player=videojs('player');
        player.persistvolume();

        async function rateVideo(type){
            data = {
                type: type,
                csrf: '<?= $csrf ?>',
                auth_token: '<?= $auth_token ?? '' ?>'
            };

            let options = {
                method: 'post',
                mode: 'cors',
                headers: {
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                },
                body: JSON.stringify(data)
            };

            let req = new Request('../videos/rateVideo', options);

            response = await fetch(req);

            data = await response.json();

            if(data == true){
                if(type == true)
                {
                    element = document.getElementById('likeCount');
                    value = parseInt(element.innerHTML);
                    element.innerHTML = value + 1;
                }
                else if(type == false){
                    element = document.getElementById('dislikeCount');
                    value = parseInt(element.innerHTML);
                    element.innerHTML = value + 1;
                }
            }
            else if(data == false){
                console.log('rating insertion failed');
            }
        }

    </script>
</body>
