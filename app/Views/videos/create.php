<?= service('validation')->listErrors(); ?>

<form class='form' enctype="multipart/form-data" action="/videos/create" method='post'>
    <?= csrf_field() ?>

    <label class='form-key' for="title">Title</label>
    <input class='form-value' type="input" name="title"/><br />

    <label class='form-key' for="file">File</label>
    <input class='form-value' type="file" name="video"><br>

    <label class='form-key' for="description">Description</label><br />
    <textarea class='form-value' rows=10 cols=50 name="description"/></textarea><br />

    <label class='form-key' for="visibility">Visibility</label>
    <input class='form-value' type="number" name="visibility"/><br />

    <input class='form-value' type="submit" name="submit" value="Add video" />
</form>