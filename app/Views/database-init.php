<?php
    $db = [
        'hostname' => 'localhost',
        'username' => 'root',
        'password' => '',
        'database' => 'tubeyou',
        'DBDriver' => 'MySQLi',
        'charset'  => 'utf8',
        'DBCollat' => 'utf8_general_ci',
        'port'     => 3306,
    ];
    
    //menu
    $forge = \Config\Database::forge($db);
    $forge->addField('id');
    $fields = [
        'name' => [
            'type' => 'varchar',
            'constraint' => '30',
        ],
        'link' => [
            'type' => 'varchar',
            'constraint' => '30',
        ]
    ];
    $forge->addField($fields);
    if ($forge->createTable('menu', true)) {
        echo "table 'menu' created.<br>";
    }

    $database = db_connect($db);

    $sql = "SELECT * FROM `menu`";
    $query = $database->query($sql);
    if ($query->getNumRows() == 0) {
        $sql = "INSERT INTO `menu` (`id`, `name`, `link`) VALUES
                (NULL, 'Videos', '/videos'),(NULL, 'Create a video','/videos/create'),
                (NULL, 'Profile', '/profile'), (NULL, 'Register', '/register'),
                (NULL, 'Login', '/login'), (NULL, 'Log out', '/logout'),
                (NULL, 'Admin Panel', '/adminpanel');";
        $database->query($sql);
        echo 'added menu items<br>';
    }


    $mysqli = new mysqli("localhost","root","","tubeyou");

    // Check connection
    if ($mysqli -> connect_errno) {
        echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
    }
    else{
        echo 'connection good<br />';
    }
    
    //users

    $query = 'CREATE TABLE `tubeyou`.`users` ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT , `username` VARCHAR(255) NOT NULL , `password` VARCHAR(255) NOT NULL , `email` VARCHAR(255) NOT NULL , `profile_pic` VARCHAR(255) NOT NULL , `subscriber_count` INT UNSIGNED NOT NULL DEFAULT \'0\' , PRIMARY KEY (`id`)) ENGINE = InnoDB;';

    try{
        if($mysqli->query($query)){
            echo "table 'users' created.<br>";
        }
    }
    catch(mysqli_sql_exception $e){
        echo "table 'users' already exists.<br>";
    }
    
/* 
    $forge = \Config\Database::forge($db);
    $fields = [
        'id'          => [
            'type'           => 'INT',
            'constraint'     => 5,
            'unsigned'       => true,
            'auto_increment' => true
        ],
        'username' => [
            'type' => 'varchar',
            'constraint' => '255',
        ],
        'password' => [
            'type' => 'varchar',
            'constraint' => '255',
        ],
        'email' => [
            'type' => 'varchar',
            'constraint' => '255',
        ],
        'profile_pic' => [
            'type' => 'varchar',
            'constraint' => '255'
        ],
        'subscriber_count' => [
            'type' => 'INT',
            'unsigned' => true,
            'default' => 0
        ],
    ];
    $forge->addField($fields);
    if ($forge->createTable('users', true)) {
        echo "table 'users' created.<br>";
    }
 */
    //admins
    $forge = \Config\Database::forge($db);
    $fields = [
        'user_id' => [
            'type' => 'INT',
            'unsigned' => true,
            'unique' => true,
            'default' => false
        ],
    ];
    $forge->addField($fields);
    if ($forge->createTable('admins', true)) {
        echo "table 'admins' created.<br>";
    }

    //videos
    
    $query = 'CREATE TABLE `tubeyou`.`videos` ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT , `user_id` INT UNSIGNED NOT NULL , `title` VARCHAR(255) NOT NULL , `filename` VARCHAR(255) NOT NULL , `description` TEXT NOT NULL , `thumbnail` VARCHAR(255) NOT NULL , `views` INT UNSIGNED NOT NULL DEFAULT \'0\' , `likes` INT UNSIGNED NOT NULL DEFAULT \'0\' , `dislikes` INT UNSIGNED NOT NULL DEFAULT \'0\' , `upload_date` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP, `visibility` TINYINT UNSIGNED NOT NULL , `url` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;';

    try{
        if($mysqli->query($query)){
            echo "table 'videos' created.<br>";
        }
    }
    catch(mysqli_sql_exception $e){
        echo "table 'videos' already exists.<br>";
    }
    
    
/*     $forge = \Config\Database::forge($db);
    $fields = [
        'id' => [
            'type' => 'INT',
            'unsigned' => true,
            'auto_increment' => true
        ],
        'user_id' => [
            'type' => 'INT',
            'unsigned' => true,
        ],
        'title' => [
            'type' => 'VARCHAR',
            'constraint' => '255'
        ],
        'filename' => [
            'type' => 'VARCHAR',
            'constraint' => '255'
        ],
        'description' => [
            'type' => 'TEXT'
        ],
        'thumbnail' => [
            'type' => 'VARCHAR',
            'constraint' => '255'
        ],
        'views' => [
            'type' => 'INT',
            'unsigned' => true,
            'default' => 0
        ],
        'likes' => [
            'type' => 'INT',
            'unsigned' => true,
            'default' => 0
        ],
        'dislikes' => [
            'type' => 'INT',
            'unsigned' => true,
            'default' => 0
        ],
        'upload_date' => [
            'type' => 'date',
        ],
        'visibility' => [
            'type' => 'TINYINT',
            'unsigned' => true,
        ],
        'url' => [
            'type' => 'VARCHAR',
            'constraint' => '255'
        ],
    ];
    $forge->addField($fields);
    if ($forge->createTable('videos', true)) {
        echo "table 'videos' created.<br>";
    }
 */
    //subscriptions
    $forge = \Config\Database::forge($db);
    $fields = [
        'user_id' => [
            'type' => 'INT',
            'unsigned' => true,
        ],
        'recipient_id' => [
            'type' => 'INT',
            'unsigned' => true,
        ]
    ];
    $forge->addField($fields);
    if ($forge->createTable('subscriptions', true)) {
        echo "table 'subscriptions' created.<br>";
    }

    //playlist_videos
    $forge = \Config\Database::forge($db);
    $fields = [
        'playlist_id' => [
            'type' => 'INT',
            'unsigned' => true,
        ],
        'video_id' => [
            'type' => 'INT',
            'unsigned' => true,
        ]
    ];
    $forge->addField($fields);
    if ($forge->createTable('playlist_videos', true)) {
        echo "table 'playlist_videos' created.<br>";
    }

    $query = 'CREATE TABLE `tubeyou`.`ratings` ( `user_id` INT UNSIGNED NOT NULL , `video_id` INT UNSIGNED NOT NULL , `type` BOOLEAN NOT NULL , PRIMARY KEY (`video_id`, `user_id`)) ENGINE = InnoDB;';

    try{
        if($mysqli->query($query)){
            echo "table 'ratings' created.<br>";
        }
    }
    catch(mysqli_sql_exception $e){
        echo "table 'ratings' already exists.<br>";
    }

/*  //ratings
    $forge = \Config\Database::forge($db);
    $fields = [
        'user_id' => [
            'type' => 'INT',
            'unsigned' => true
        ],
        'video_id' => [
            'type' => 'INT',
            'unsigned' => true
        ],
        'type' => [
            'type' => 'BOOL'
        ]
    ];
    $forge->addField($fields);
    if ($forge->createTable('ratings', true)) {
        echo "table 'ratings' created.<br>";
    }
 */
    //comments
    $forge = \Config\Database::forge($db);
    $fields = [
        'id' => [
            'type' => 'INT',
            'unsigned' => true,
            'auto_increment' => true
        ],
        'user_id' => [
            'type' => 'INT',
            'unsigned' => true,
        ],
        'video_id' => [
            'type' => 'INT',
            'unsigned' => true,
        ],
        'body' => [
            'type' => 'TEXT'
        ],
        'parent' => [
            'type' => 'INT',
            'unsigned' => true,
            'null' => true
        ]
    ];
    $forge->addField($fields);
    if ($forge->createTable('comments', true)) {
        echo "table 'comments' created.<br>";
    }

    //playlists
    $forge = \Config\Database::forge($db);
    $forge->addField('id INT(9) NOT NULL AUTO_INCREMENT');
    $fields = [
        'user_id' => [
            'type' => 'INT',
            'unsigned' => true,
        ],
        'visibility' => [
            'type' => 'TINYINT',
            'unsigned' => true,
        ],
        'title' => [
            'type' => 'VARCHAR',
            'constraint' => '255'
        ],
        'views' => [
            'type' => 'INT',
            'unsigned' => true,
            'default' => 0
        ],
    ];
    $forge->addField($fields);
    if ($forge->createTable('playlists', true)) {
        echo "table 'playlists' created.<br>";
    }
?>