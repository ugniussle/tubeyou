<style>
    table{
        border-collapse:collapse;
        border-spacing: 0;
        border: 2px solid black;
        margin:auto;
        margin-top: 5px;
    }
    td{
        padding:5px;
        border: 1px solid black;
    }
</style>

<table>

<?php foreach($user as $key => $value): ?>

    <tr>
    <?php if($key != 'password' && $key != 'id'): ?>
        <?= '<td>'.ucfirst($key).'</td> <td>'.$value.'</td>' ?>
    <?php endif ?>
    </tr>

<?php endforeach; ?>

</table>