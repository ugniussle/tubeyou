<style>
    .linksubmit{
        border:0;
        background-color: rgba(0,0,0,0);
        color:red;
        font-size: 20.8px;
        margin:0;
        padding: 0;
        font-family: 'Times New Roman';
    }

    .linksubmit:hover{
        cursor: pointer;
    }
    @keyframes prevent-delete{
        0% {pointer-events: none;background-color: green;}
        50% {pointer-events: none;background-color: lightgreen;}
        100% {pointer-events: all;background-color: white;}
    }
    .delete:hover{
        animation: prevent-delete 4s;
        animation-fill-mode: forwards;
    }
</style>
<h1 class='header-title'>User: <?= $user['username'] ?></h1><br>

<form class='form' action="/adminpanel" method="post">
    <?php foreach($user as $key => $value): ?>

        <?php if($key == 'id'): ?>

            <li class='form-key'><?= ucfirst($key) ?></li>
            <input class='form-value' readonly type='text' name="<?= $key ?>" value="<?= $value ?>">
            <br>
        
        <?php endif ?>

        <?php if($key !='id' && $key != 'password'): ?>

            <li class='form-key'><?= ucfirst($key) ?></li>
            <input class='form-value' type='text' name="<?= $key ?>" value="<?= $value ?>">
            <br>

        <?php endif ?>

    <?php endforeach; ?>
    <?= csrf_field() ?>
    <?= service('validation')->listErrors(); ?>
    <input type='submit' style='margin-right: 71.5%;margin-left: 1%' name='change' class='linksubmit' value='Submit'>
    <input type='submit' name='delete' class='linksubmit delete' value='Delete'>
    </form>