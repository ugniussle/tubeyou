<form class='form' action="/register" method="post">
    <?= csrf_field() ?>
    <?= service('validation')->listErrors(); ?>

    <label class='form-key' for="username">Username</label>
    <input class='form-value' type="input" name="username" value='<?= $username ?? '' ?>' /><br />

    <label class='form-key' for="password">Password</label>
    <input class='form-value' type="password" name="password"/><br />

    <label class='form-key' for="pass_confirm">Confirm password</label>
    <input class='form-value' type="password" name="pass_confirm"/><br />

    <label class='form-key' for="email">Email</label>
    <input class='form-value' type="input" name="email" value='<?= $email ?? '' ?>' /><br />

    <input class='form-value' type="submit" name="submit" value="Register!"><br />
</form>