<?php

namespace App\Controllers;

use App\Models\RatingModel;
use App\Models\VideosModel;
use ErrorException;
use CodeIgniter\HTTP\URI;
use InvalidArgumentException;

class Videos extends BaseController{
    public function index(){
        $model = model(VideosModel::class);

        $data = [
            'videos' => $model->getVideos(),
            'title' => 'Videos',
        ];

        $this->displayPage('videos/overview',$data,1,1,0,0);

    }
    public function view($slug = false){
        $model = model(VideosModel::class);

        try{
            $data['video'] = $model->getVideos($slug)[0];
        } catch(ErrorException $e) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Cannot find page: ' . $slug);
            exit();
        }

        $data['csrf'] = csrf_hash();

        $model->increaseViews($slug);

        $this->displayPage('videos/view', $data, 1, 1, 0, 0, $data['video']['title']);
    }

    public function rateVideo(){
        $uri = new URI($this->request->header('Referer'));

        $data = $this->request->getJSON(true);

        if($this->verifyAjaxRequest($uri, $data['auth_token'], $data['csrf']) === true){
            $model = new VideosModel();

            try{
                $video = $model->getVideos($uri->getSegment(5))[0];
            }
            catch(InvalidArgumentException){
                echo json_encode('bad url');
                exit();
            }

            if(isset($data['type']) && ($data['type'] == true || $data['type'] == false))
                $newRating = $data['type'];
            else{
                echo json_encode('bad type');
                exit();
            }

            $ratingModel = new RatingModel();

            $ratingModel->updateRatings(intval($video['id']));

            $user_id = session()->get('current_user_id');

            if($user_id == null){
                log_message('error', "failed getting user_id");
            }

            $user_id = intval($user_id);
            $video_id = intval($video['id']);

            $currentRating = $ratingModel->getRatingType($user_id, $video_id);
            if($currentRating === null){
                $currentRating = 2;
            }

            if(intval($currentRating) === intval($newRating)){
                $ratingModel->deleteRating($user_id, $video_id);

                if($ratingModel){
                    echo json_encode('rating removed successfully');
                }
                exit();
            }

            elseif(intval($currentRating) !== intval($newRating)){
                $ratingModel->deleteRating($user_id, $video_id);
            }

            $ret = $ratingModel->insertRating($user_id, $video_id, $newRating);

            if(!$ret){
                log_message('error', 'Rating insertion failed.');
            }
            
            echo json_encode($ret);
            exit();
        }
    }

    private function verifyAjaxRequest($uri, $auth_token, $csrf_token){
        $request = $this->request;

        if($request->isAJAX() !== true){
            echo 'request is not ajax';
            return false;
        }

        if($request->getMethod() !== 'post'){
            echo 'request is not post';
            return false;
        }

        if($uri->getSegment(3) !== 'localhost:8080'){
            echo 'request is from somewhere else';
            return false;
        }

        if($this->authenticate($auth_token, $csrf_token) != true){
            return false;
        }

        return true;
    }

    public function create(){
        $model = model(VideosModel::class);

        $session = session();

        if( ($session->get('current_user_name') ?? '') == ''){
            $this->displayPage(
                'user/login',
                [
                    'message'=>'To upload a video you must log in first.',
                    'title'  =>'Login'
                ]
            );

            exit();
        }

        if($this->request->getMethod() === 'post' && $this->validate([
            'title' => 'required|min_length[3]|max_length[50]|no_special_chars',
            'description'=>'no_special_chars',
            'visibility'=>'less_than[3]|greater_than_equal_to[0]'
            ])){
            $file = $this->request->getFile('video');

            if($file == null){
                log_message('error', "file is null");
            }

            if(!$file->isValid()){
                throw new \RuntimeException($file->getErrorString().'('.$file->getError().')');
            }

            $type = $file->getMimeType();

            if($type != 'video/mp4' && $type != 'video/webm' && $type != 'video/mkv'){
                $this->displayPage('videos/create',['title' => 'Create a video','message'=>'bad file type']);
                exit();
            }

            $filename = $file->getRandomName();

            $basepath = WRITEPATH.'../public/';

            $file->move($basepath.'Files/Videos',$filename);

            $url = substr($filename, 0, strrpos($filename, '.'));
            
            $thumbnail = $url.'.jpg';

            $command = 'ffmpeg -ss 00:00:00 -i '.$basepath.'Files/Videos/'.$filename.' -vf "scale=1024:576:force_original_aspect_ratio=decrease,pad=1024:576:(ow-iw)/2:(oh-ih)/2,setsar=1" -vframes 1 '.$basepath.'Files/Thumbnails/'.$thumbnail;
            shell_exec($command);

            $model->save([
                'title' =>          $this->request->getPost('title'),
                'url' =>            $url,
                'filename' =>       $filename,
                'user_id' =>        $session->get('current_user_id'),
                'thumbnail' =>      $thumbnail,
                'description' =>    $this->request->getPost('description'),
                'visiblity' =>      0,
                'upload_date' =>    date('y-m-d', time())
            ]);

            header('Location: /videos');
            exit();
        }
        else{
            echo view('templates/form_style.php');
            $this->displayPage('videos/create',['title' => 'Create a video'],1,1,0,1);
        }
    }
}
?>