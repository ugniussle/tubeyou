<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */
class BaseController extends Controller
{
    /**
     * Instance of the main Request object.
     *
     * @var CLIRequest|IncomingRequest
     */
    protected $request;

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var array
     */
    protected $helpers = [];

    /**
     * Constructor.
     */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        // Preload any models, libraries, etc, here.

        // E.g.: $this->session = \Config\Services::session();
    }
    protected function displayPage($page, $data, $menu=1, $footer=1,$sidebar=1, $header=1, $title=''){
        $session = session();

        if($session->get('login') === true){
            $auth = $this->getAuthToken($session->get('user_id'));

            $session->set('auth_key',$auth[0]);
    
            $data['auth_token'] = $auth[1];
        }
        
        if($title == '')
            echo '<title>Tubeyou</title>';

        else
            echo '<title>'.$title.'</title>';
        
        if($session->get('login') == false)$sidebar=0;
        
        $data['menu'] = model(MenuModel::class)->getMenu();

        $data['sidebar'] = $sidebar;

        if($menu)       echo view('templates/menu_bar',$data);
        if($sidebar)    echo view('templates/sidebar_right',$data);

        echo '<style>.body-container{height:95%;top:0;overflow-y:auto;';
        if($sidebar==1)echo 'width:80%';
        else echo 'width:100%';
        echo '}</style>';

        if($header)     echo view('templates/header', $data);

        echo '<div class="body-container">';

        echo view($page, $data);

        echo '</div>';

        if($footer)     echo view('templates/footer', $data);
    }

    private function getAuthToken($user_id){
        $session_id = session_id();

        $current_time = strval(time());

        $original = $session_id.$current_time.$user_id;

        $auth_token = password_hash($original, PASSWORD_DEFAULT);

        return [$original, $auth_token];
    }

    protected function authenticate($auth_token, $csrfHash){
        $auth_key = session()->get('auth_key');

        if(session()->get('login') === true){
            if(password_verify($auth_key, $auth_token)){
                if($csrfHash == csrf_hash()){
                    log_message('debug', 'authenticated successfully');
                    return true;
                }

                else{
                    echo json_encode('csrf tokens do not match');
                    return false;
                }
            }

            else{
                echo json_encode('auth tokens do not match');
                return false;
            }
        }

        else{
            echo json_encode('not logged in');
            return false;
        }
    }
}