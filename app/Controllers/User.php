<?php

namespace App\Controllers;

use App\Models\adminModel;
use App\Models\userModel;

class User extends BaseController{

    public function index(){

        $session = session();
        if(($session->get('login') ?? false) != true){
            header('Location: home');
            exit();
        }

        $model = model(UserModel::class);

        $data = [
            'title' => 'Profile',
            'user' => $model->getUserId($_SESSION['current_user_id'])
        ];

        $this->displayPage('user/profile',$data,1,1,0,0);
    }

    public function view($user){
        $model = model(VideosModel::class);
        $data = [
            'videos' => $model->getUserVideos($user),
            'title' => $user.'\'s videos:'
        ];
        $this->displayPage('videos/overview',$data,1,1,0,1);
    }

    public function register(){
        $model = model(UserModel::class);

        if($this->request->getMethod() === 'post' && $this->validate([
            'username'    => 'required|min_length[5]|max_length[30]|is_unique[users.username]|no_special_chars',
            'password'    => 'required|min_length[5]|max_length[50]|no_special_chars',
            'pass_confirm'=> 'required|matches[password]',
            'email'       => 'required|valid_email|max_length[50]|is_unique[users.email]|no_special_chars',
        ])){
            $model->save([
                'username'          => $this->request->getPost('username'),
                'password'          => password_hash($this->request->getPost('password'),PASSWORD_DEFAULT),
                'email'             => $this->request->getPost('email', FILTER_SANITIZE_EMAIL),
                'profile_pic'       => '',
                'subscriber_count'  => 0
            ]);
            header('Location: login');
            exit();
        }
        else{
            $data = [
                'title' => 'Create an account',
                'username'  => $this->request->getPost('username'),
                'email'     => $this->request->getPost('email'),
            ];
            echo view('templates/form_style.php');
            $this->displayPage('user/register',$data,1,1,0);
        }
    }

    public function login(){
        $session = session();
        session_regenerate_id(true);

        $model = new userModel();

        if($this->request->getMethod() === 'post' && $this->validate([
            'username'    => 'required|min_length[5]|max_length[30]',
            'password'    => 'required|min_length[5]|max_length[50]',
        ])){
            $user = $model->getUser($this->request->getPost('username'));

            if(!$user){
                $msg = 'Wrong username or password, try again.';
            }
            else if(password_verify($this->request->getPost('password'), $user['password'])){
                $session->set([
                    'login' => true,
                    'current_user_id' => $user['id'],
                    'current_user_name' => $user['username'],
                ]);

                if(model(AdminModel::class)->isAdmin($user['id'], $user['username']) == true){
                    echo $user['id'].' is an admin.';
                    $session->set( ['admin' => true] );
                }
                
                header("Location: home");
                exit();
            }
            else $msg = 'Wrong username or password, try again.';
        }

        $data = ['title' => 'Login', 'message' => $msg ?? ''];
        echo view('templates/form_style.php');
        $this->displayPage('user/login', $data, 1, 1, 0);
    }
    
    public function logout(){
        $session = session();
        $session->destroy();

        header("Location: home");
        exit();
    }
    public function adminPanel(){

        $model = model(UserModel::class);
        $adminModel = model(AdminModel::class);

        if(session()->get('admin') != 1){
            header("Location: home");
            exit();
        }

        if($this->request->getMethod() === 'post'){
            $user = $model->getUserId($_POST['id']);

            if($adminModel->isAdmin($user['id'], $user['username']) == true){
                header('Location: adminpanel');
                exit();
            }

            if(isset($_POST['change'])){
                $data =[
                    'id'       => 'required',
                    'username' => 'required|min_length[5]|max_length[30]|is_unique[users.username,id,'.intval($user['id']).']',
                    'email'    => 'required|valid_email|max_length[50]|is_unique[users.email,id,'.$user['id'].']',
                    'country'  => 'required|max_length[40]',
                    'city'     => 'required|max_length[40]',
                    'gender'   => 'required|max_length[15]'
                ];
                if($this->validate($data)){
                    $model->save([
                        'id'        => $this->request->getPost('id'),
                        'username'  => $this->request->getPost('username'),
                        'email'     => $this->request->getPost('email'),
                        'country'   => $this->request->getPost('country'),
                        'city'      => $this->request->getPost('city'),
                        'gender'    => $this->request->getPost('gender')
                    ]);
                }
            }
            else if(isset($_POST['delete'])){
                echo $model->deleteUser(intval($_POST['id']));
            }
            
        }
        
        $model=model(UserModel::class);

        $data = [
            'title'=> 'Admin Panel',
            'users' => $model->getInfo()
        ];
        $this->displayPage('/user/adminpanel',$data,1,1,0);

    }

    public function editUser($id = false){

        $id = intval($id);
        if($id == false || !is_int($id) || $id == 0){
            header('Location: /adminpanel');
            exit();
        }

        $session = session();
        if(($session->get('admin') != 1)){
            header('Location: home');
            exit();
        }

        $model = model(UserModel::class);
        $data = [
            'user'=>$model->getUserId($id),
            'title'=>'Admin Panel'
        ];

        
        if( ($data['user']['id'] ?? 0) != $id )echo 'user does not exist';

        echo view('templates/form_style.php');
        $this->displayPage('user/editUser',$data,1,1,0);

    }
}
?>