# Tubeyou

## What is Tubeyou?

Tubeyou is a website made with the CodeIgniter4 framework as an exam project. It is supposed to be a video sharing website (kind of like Youtube).
Currently you can create an account, upload, watch and rate (like, dislike) videos.

## Demos

[Demo1](https://youtu.be/HhkdsiOt1H0)
[Demo2](https://youtu.be/eCr-DMS_rwI)

## Development

1. clone repository
2. rename env file to .env
3. in the .env change database name to tubeyou and set username and password to your mysql server credentials
4. add ffmpeg executable to `public` folder
5. start mysql in xampp
6. run command `php spark serve` in `tubeyou` folder
7. website can be accessed at `localhost:8080`
8. go to `localhost:8080/database-init` to initialize the database

## Server Requirements

PHP version 7.3 or higher is required, with the following extensions installed:

- [intl](http://php.net/manual/en/intl.requirements.php)
- [libcurl](http://php.net/manual/en/curl.requirements.php) if you plan to use the HTTP\CURLRequest library
- [mbstring](http://php.net/manual/en/mbstring.installation.php)

Additionally, make sure that the following extensions are enabled in your PHP:

- json (enabled by default - don't turn it off)
- xml (enabled by default - don't turn it off)
- [mysqlnd](http://php.net/manual/en/mysqlnd.install.php)
